# Configuration
By default there you need to create a record type named _Augmentation_ on the CloudKit dashboard. No additional configuration is necessary, the attributes are created automatically on first write of the data. Optionally you need to create a record type named _AugmentationItems_.

## Advanced configuration

For an example configuration as shown below:

```
        ARCloudAugmentationConfig.AugmentationRecordTypeName = "space"
        ARCloudAugmentationConfig.AugmentationItemRecordTypeName = "item"
        ARCloudAugmentationConfig.AugmentationReferenceName = "space"
        ARCloudAugmentationConfig.MetadataFieldName = "jsonData"
```

Configure CloudKit items like the following:


[Augmentation: Space](archi_space_setup.jpg)

[Augmentation item: Item](archi_item_setup.jpg)
