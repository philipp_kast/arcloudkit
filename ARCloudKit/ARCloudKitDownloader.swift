//
//  ARUploadCloudKit.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 20.09.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

// Povides low level functionality to download data from CloudKit
public class ARCloudKitDownloader: ARCloudKitBase {
    public override init(scope: CKDatabase.Scope) {
        super.init(scope: scope)
    }

    public class DownloadedAsset {
        public let id: String
        public let url: URL?
        init(id: String, url: URL?) {
            self.id = id
            self.url = url
        }
    }

    private func downloadAssets(
        records: [CKRecord]?,
        directoryName: String? = nil,
        assetFields: [String: String],
        assetLists: [String: String] = [:],
        errorCallback: @escaping (String) -> Void
    ) -> [DownloadedAsset] {
        var downloadedAssets = [DownloadedAsset]()
        records?.forEach({ (record: CKRecord) in
            downloadedAssets.append(contentsOf: ARCloudKitDownloader.saveRecordAssets(
                record,
                directoryName: directoryName,
                assetFields,
                assetLists,
                errorCallback: errorCallback
            ))
        })
        return downloadedAssets
    }

    // Reads all records of CloudKid record type in the public database, downloads the asset (binary data) of multiple fields and writes them locally in files.
    // The files are named following a name field and a provided extension and stored in the users document directory.
    // NOTE: Obviously this expects unique nameField in the record type, otherwise it will overwrite files of the same name
    // Record recordType
    //      nameField (String)
    //      assetFieldN (Asset)
    // --------> Local file: nameField.localFileNameExtensionN
    public func downloadAllAssets(
        recordTypeName: String,
        name: String?,
        directoryName: String? = nil,
        assetFields: [String: String],
        assetLists: [String: String] = [:],
        complete: @escaping ([DownloadedAsset]) -> Void,
        errorCallback: @escaping (String) -> Void
    ) {
        var predicate = NSPredicate(value: true)
        if let name = name {
            predicate = NSPredicate(format: "recordID = %@", CKRecord.ID(recordName: name))
        }
        let query = CKQuery(recordType: recordTypeName, predicate: predicate)
        self.database.perform(query, inZoneWith: nil) { records, error in
            if let error = error {
                // Insert error handling
                let message = "Error query data: \(error)"
                ErrorReporter.reportUnexpectedError(message: message)
                errorCallback(message)
                return
            }
            complete(self.downloadAssets(
                records: records,
                directoryName: directoryName,
                assetFields: assetFields,
                assetLists: assetLists,
                errorCallback: errorCallback
            ))
        }
    }

    private func prepareReferenceQuery(
        recordTypeName: String,
        referenceField: String,
        referenceName: String
    ) -> (CKQuery, CKRecordZone.ID?) {
        var refId = CKRecord.ID(recordName: referenceName)
        var zoneID: CKRecordZone.ID? = nil
        if scope == CKDatabase.Scope.private {
            zoneID = CKRecordZone.ID(zoneName: ZoneName, ownerName: CKCurrentUserDefaultName)
            refId = CKRecord.ID(recordName: referenceName, zoneID: zoneID!)
        }
        let ref = CKRecord.Reference(recordID: refId, action: CKRecord_Reference_Action.none)
        let predicate = NSPredicate(format: "%K == %@", referenceField, ref)
        return (CKQuery(recordType: recordTypeName, predicate: predicate), zoneID)
    }

    // Downloads values of a reference of a recordType
    // NOTE: All records of a record type which have a given reference are fetched and the values to be read are returned in the completion handler
    public func downloadAllValuesOfReference(
        recordTypeName: String,
        referenceField: String,
        referenceName: String,
        valueNamesToRead: [String],
        completed: @escaping ([[String: AnyObject]]) -> Void
    ) {
        let (query, zoneID) = prepareReferenceQuery(
            recordTypeName: recordTypeName,
            referenceField: referenceField,
            referenceName: referenceName
        )
        self.database.perform(query, inZoneWith: zoneID) { records, error in
            if let error = error {
                ErrorReporter.reportUnexpectedError(message: "Error query data: \(error)")
                return
            }
            var recordValues = [[String: AnyObject]]()
            records?.forEach({ (record: CKRecord) in
                var values = [String: AnyObject]()
                for valueNameToRead in valueNamesToRead {
                    values[valueNameToRead] = record[valueNameToRead]
                }
                recordValues.append(values)
            })
            completed(recordValues)
        }
    }

    public func downloadAllAssetsOfReference(
        recordTypeName: String,
        referenceField: String,
        referenceName: String,
        directoryName: String? = nil,
        assetFields: [String: String],
        assetLists: [String: String] = [:],
        complete: @escaping ([DownloadedAsset]) -> Void,
        errorCallback: @escaping (String) -> Void
    ) -> Void {
        let (query, zoneID) = prepareReferenceQuery(
            recordTypeName: recordTypeName,
            referenceField: referenceField,
            referenceName: referenceName
        )
        self.database.perform(query, inZoneWith: zoneID) { records, error in
            if let error = error {
                ErrorReporter.reportUnexpectedError(message: "Error query data: \(error)")
                return
            }
            complete(self.downloadAssets(
                records: records,
                directoryName: directoryName,
                assetFields: assetFields,
                assetLists: assetLists,
                errorCallback: errorCallback
            ))
        }
    }

    private func queryAll(
        recordTypeName: String,
        completionHandler: @escaping ([CKRecord]?, Error?) -> Void
    ) {
        let query = CKQuery(recordType: recordTypeName, predicate: NSPredicate(value: true))
        self.database.perform(query, inZoneWith: nil) { (records, error) in
            records?.forEach({ (record) in

                // System Field from property
                let recordName_fromProperty = record.recordID.recordName
                print("System Field, recordName: \(recordName_fromProperty)")

                // Custom Field from key path (eg: deeplink)
                let deeplink = record.value(forKey: "deeplink")
                print("Custom Field, deeplink: \(deeplink ?? "")")
            })
        }
    }

    public func queryWithinDistance(
        recordTypeName: String,
        location: CLLocation,
        withinMeters: Float,
        fieldNames: [String],
        completionHandler: @escaping ([ARCloudKitQueryResults], Error?) -> Void
    ) {
        let predicate = NSPredicate(format: "distanceToLocation:fromLocation:(location, %@) < %f", location, withinMeters)
        query(recordTypeName: recordTypeName, fieldNames: fieldNames, predicate: predicate, completionHandler: completionHandler)
    }

    public func queryAllNames(
        recordTypeName: String,
        completionHandler: @escaping ([String], Error?) -> Void
    ) {
        query(
            recordTypeName: recordTypeName,
            fieldNames: [ARCloudAugmentationConfig.NameFieldName],
            predicate: NSPredicate(value: true),
            completionHandler: { names, error in
                var n = [String]()
                if error == nil {
                    n = names.map { sn in sn.recordID.recordName }
                }
                completionHandler(n, error)
            }
        )
    }

    public func queryAll(recordTypeName: String, fieldNames: [String], completionHandler: @escaping ([ARCloudKitQueryResults], Error?) -> Void) {
        query(recordTypeName: recordTypeName, fieldNames: fieldNames, predicate: NSPredicate(value: true), completionHandler: completionHandler)
    }

    public func exists(
        recordName: String,
        complete: @escaping (Bool, Error?) -> Void
    ) {
        queryRecordByName(recordName: recordName, complete: { record, error in
            complete(record != nil, error)
        })
    }

    public func queryRecordByName(
        recordName: String,
        complete: @escaping (CKRecord?, Error?) -> Void
    ) {
        var recordID = CKRecord.ID(recordName: recordName)
        if scope == CKDatabase.Scope.private || scope == CKDatabase.Scope.shared {
            let zoneID = CKRecordZone.ID(zoneName: ZoneName, ownerName: CKCurrentUserDefaultName)
            recordID = CKRecord.ID(recordName: recordName, zoneID: zoneID)
        }

        self.database.fetch(withRecordID: recordID) { (record, error) in
            if let error = error, error._code != 11 {
                complete(nil, error)
                return
            }
            if record == nil {
                complete(nil, nil)
            }
            else {
                complete(record!, nil)
            }
        }
    }

    private func query(
        recordTypeName: String,
        fieldNames: [String],
        predicate: NSPredicate,
        completionHandler: @escaping ([ARCloudKitQueryResults], Error?) -> Void
    ) {
        var names = [ARCloudKitQueryResults]()

        // select all records
        let query = CKQuery(recordType: recordTypeName, predicate: predicate)

        // get just one value only
        let operation = CKQueryOperation(query: query)
        operation.desiredKeys = fieldNames

        operation.recordFetchedBlock = { (record : CKRecord) in
            // process record
            var n = [String]()
            for fieldName in fieldNames {
                if let name = record[fieldName] as? String {
                    n.append(name)
                }
                else {
                    n.append("")
                }
            }
            names.append(ARCloudKitQueryResults(recordID: record.recordID, values: n))
        }

        operation.queryCompletionBlock = { cursor, error in
            completionHandler(names, error)
        }

        // addOperation
        self.database.add(operation)
    }

    private func queryByName(name: String, completionHandler: @escaping (CKRecord?, Error?) -> Void) {
        var recordID = CKRecord.ID(recordName: name)
        if scope == CKDatabase.Scope.private {
            let zoneID = CKRecordZone.ID(zoneName: ZoneName, ownerName: CKCurrentUserDefaultName)
            recordID = CKRecord.ID(recordName: name, zoneID: zoneID)
        }

        self.database.fetch(withRecordID: recordID) { (record, error) in
            if let error = error, error._code != 11 {
                let message = "Error save data: \(error)"
                ErrorReporter.reportUnexpectedError(message: message)
            }
            completionHandler(record, error)
        }
    }

    // Downloads the asset (binary data) of multiple fields and writes them locally in files.
    // The files are named following a name field and a provided extension and stored in the users document directory.
    // NOTE: Obviously this expects unique nameField in the record type, otherwise it will overwrite files of the same name
    // Record
    //      nameField (String)
    //      assetFieldN (Asset)
    // --------> Local file: nameField.localFileNameExtensionN
    public class func saveRecordAssets(
        _ record: (CKRecord),
        directoryName: String? = nil,
        _ assetFields: [String: String],
        _ assetLists: [String: String] = [:],
        errorCallback: @escaping (String) -> Void
    ) -> [DownloadedAsset] {
        var downloadedAssets = [DownloadedAsset]()
        let name = record.recordID.recordName
        // AssetFields contains [assetField: String, localFileNameExtension: String]
        for assetField in assetFields {
            let fieldValue = record[assetField.key]
            if fieldValue == nil {
                // Null value
                downloadedAssets.append(DownloadedAsset(id: assetField.key, url: nil))
                continue
            }
            guard let asset = fieldValue as? CKAsset else {
                let message = "Error reading field"
                ErrorReporter.reportUnexpectedError(message: message)
                errorCallback(message)
                return downloadedAssets
            }
            if let downloadedAsset = self.downloadAsset(asset: asset, name: name, directoryName: directoryName, extensionOfLocalFile: assetField.value, numberOfFileInLocalName: 0) {
                downloadedAssets.append(downloadedAsset)
            }
            else {
                errorCallback("Error downloading asset")
                return downloadedAssets
            }
        }
        for assetList in assetLists {
            guard let assets = record[assetList.key] as? [CKAsset] else {
                let message = "Error reading field list"
                ErrorReporter.reportUnexpectedError(message: message)
                errorCallback(message)
                return downloadedAssets
            }
            var numberOfAsset = 0
            for asset in assets {
                if let downloadedAsset = self.downloadAsset(asset: asset, name: name, directoryName: directoryName, extensionOfLocalFile: assetList.value, numberOfFileInLocalName: numberOfAsset) {
                    downloadedAssets.append(downloadedAsset)
                    numberOfAsset = numberOfAsset + 1
                }
                else {
                    errorCallback("Error downloading asset")
                    return downloadedAssets
                }
            }
        }
        return downloadedAssets
    }

    private class func ensureBaseURL(_ dir: URL) {
        let fileManager = FileManager.default
        var isDirectory: ObjCBool = false
        let exists = fileManager.fileExists(atPath: dir.path, isDirectory: &isDirectory)
        if !exists {
            do {
                try fileManager.createDirectory(at: dir, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("Failed to create file: \(error)")
            }
        }
    }

    private class func baseURL(_ directoryName: String?) -> URL? {
        if var dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            if let d = directoryName {
                dir = dir.appendingPathComponent(d, isDirectory: true)
                ensureBaseURL(dir)
            }
            return dir
        }
        return nil
    }

    // Downloads an asset from field in cloud kit and saves it in a local file with file name using a provided extension, and allows to add a number to the file name
    private class func downloadAsset(asset: CKAsset, name: String, directoryName: String?, extensionOfLocalFile: String, numberOfFileInLocalName: Int) -> DownloadedAsset? {
        let assetData: Data
        do {
            assetData = try Data(contentsOf: asset.fileURL)
        } catch {
            let message = "Error downloading asset: \(error)"
            ErrorReporter.reportUnexpectedError(message: message)
            return nil
        }
        if let dir = baseURL(directoryName) {
            let fileName = name + (numberOfFileInLocalName > 0 ? String(numberOfFileInLocalName) : "") + "." + extensionOfLocalFile
            let url = dir.appendingPathComponent(fileName)
            do {
                try assetData.write(to: url)
            }
            catch {
                let message = "Error writing to file: \(error)"
                ErrorReporter.reportUnexpectedError(message: message)
                return nil
            }
            return DownloadedAsset(id: name, url: url)
        }
        return nil
    }
}
