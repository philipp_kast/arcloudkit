//
//  ARCloudError.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 14.11.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

import Foundation

public enum ARCloudError: Error {
    case error(String)
}
