//
//  ARCloudKitDeleter.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 22.10.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

public class ARCloudKitDeleter: ARCloudKitBase {
    public override init(scope: CKDatabase.Scope) {
        super.init(scope: scope)
    }

    // Delete a record with id
    public func delete(
        name: String,
        completed: @escaping () -> Void,
        errorCallback: @escaping (Error) -> Void
    ) {
        self.deleteRecord(
            name: name,
            completed: completed,
            errorCallback: { error in
                if (nil != ARCloudKitRetry.retryCloudKitOperationIfPossible(
                    with: error,
                    block: {
                        self.delete(
                            name: name,
                            completed: completed,
                            errorCallback: errorCallback
                        )
                    }
                )) {
                    errorCallback(error)
                }
            }
        )
    }


    private func deleteRecord(
        name: String,
        completed: @escaping () -> Void,
        errorCallback: @escaping (Error) -> Void
    ) {
        var recordID = CKRecord.ID(recordName: name)
        if scope == CKDatabase.Scope.private {
            let zoneID = CKRecordZone.ID(zoneName: ZoneName, ownerName: CKCurrentUserDefaultName)
            recordID = CKRecord.ID(recordName: name, zoneID: zoneID)
        }
        database.delete(withRecordID: recordID) { (id, error) in
            if let e = error {
                print("Failed to delete record \(name): \(e)")
                errorCallback(e)
                return
            }
            if id == nil {
                errorCallback(ARCloudError.error("No record has been deleted for \(name):"))
                return
            }
            completed()
        }
    }
}
