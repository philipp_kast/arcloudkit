//
//  ARCloudDirectory.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

public protocol ARCloudDirectory {
    var directoryName: String? { get set }
    func baseURL() -> URL?
}

extension ARCloudDirectory {
    public func baseURL() -> URL? {
        if var dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            if let d = directoryName {
                dir = dir.appendingPathComponent(d, isDirectory: true)
                ensureBaseURL(dir)
            }
            return dir
        }
        return nil
    }

    private func ensureBaseURL(_ dir: URL) {
        let fileManager = FileManager.default
        var isDirectory: ObjCBool = false
        let exists = fileManager.fileExists(atPath: dir.path, isDirectory: &isDirectory)
        if !exists {
            do {
                try fileManager.createDirectory(at: dir, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("Failed to create file: \(error)")
            }
        }
    }
}
