//
//  ARCloudRegAugmentation.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//
import Foundation
import CoreLocation
import CloudKit
import CoreLocation

public class ARCloudRegAugmentation: ARCloudRegSavableAugmentation {
    let worldMapSession: ARCloudRegWorldMapSession
    let worldMap: ARCloudRegWorldMap
    let screenShots: [URL]
    let location: CLLocation?

    public init(
        worldMapSession: ARCloudRegWorldMapSession,
        worldMap: ARCloudRegWorldMap,
        screenShots: [URL],
        location: CLLocation?
        ) {
        self.worldMapSession = worldMapSession
        self.worldMap = worldMap
        self.screenShots = screenShots
        self.location = location
    }

    public func save(
        saveProvider: @escaping (
        _ worldMapUrl: URL,
        _ metadataUrl: URL,
        _ ifcUrl: URL?,
        _ screenShots: [URL],
        _ location: CLLocation?,
        _ pointsOfInterest: [ARCloudRegWorldMapPointOfInterest],
        _ completionHandler: @escaping (Error?) -> Void
        ) -> Void,
        completionHandler: @escaping (Error?) -> Void
        ) {
        do {
            try worldMap.shareSession(session: worldMapSession) { error in
                if let error = error {
                    completionHandler(error)
                }
                if let worldMapUrl = self.worldMap.urlForSessionKey(sessionKey: self.worldMapSession.key),
                    let metadataUrl = self.worldMap.metadataUrlForSessionKey(sessionKey: self.worldMapSession.key) {
                    let ifcUrl = self.worldMap.ifcUrlForSessionKey(sessionKey: self.worldMapSession.key)
                    saveProvider(
                        worldMapUrl,
                        metadataUrl,
                        ifcUrl,
                        self.screenShots,
                        self.location,
                        self.worldMapSession.pointsOfInterest,
                        completionHandler
                    )
                }
            }
        }
        catch {
            print("Error writing session \(error)")
            completionHandler(error)
        }
    }
}
