//
//  ARCloudStateExtension.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import ARKit

extension ARFrame.WorldMappingStatus: CustomStringConvertible {
    public var description: String {
        switch self {
        case .notAvailable:
            return "Not Available"
        case .limited:
            return "Limited"
        case .extending:
            return "Extending"
        case .mapped:
            return "Mapped"
        }
    }
}

extension ARCamera.TrackingState: CustomStringConvertible {
    public var description: String {
        switch self {
        case .notAvailable:
            return "Not available"
        case .limited(.excessiveMotion):
            return "Limited - excessive Motion"
        case .limited(.insufficientFeatures):
            return "Limited - insufficient features"
        case .limited(.initializing):
            return "Limited - initializing"
        case .limited(.relocalizing):
            return "Limited - relocalizing"
        case .normal:
            return "Normal"
        }
    }
}
