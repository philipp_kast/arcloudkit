//
//  ARCloudSessionPersistence.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

public protocol ARCloudSessionPersictence: ARCloudDirectory {
    var fileExtension: String { get }
    func sessionKeys() throws -> [String]
    func sessionKeysAndNames() throws -> [(String, String)]
    func sessionExists(sessionKey: String) throws -> Bool
    func sessionExistsNoThrow(sessionKey: String) -> Bool
}

extension ARCloudSessionPersictence {
    public func sessionKeys() throws -> [String] {
        var sessionKeys = [String]()
        let documentsUrl = baseURL()!
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            let files = directoryContents.filter{ $0.pathExtension == self.fileExtension }
            sessionKeys = files.map{ $0.deletingPathExtension().lastPathComponent }
        } catch {
            throw ARCloudWriteError.error(error.localizedDescription)
        }
        return sessionKeys
    }

    public func sessionKeysAndNames() throws -> [(String, String)] {
        var names = [(String, String)]()
        let keys = try sessionKeys()
        let wm = ARCloudRegWorldMapBase()
        for key in keys {
            do {
                let o = try wm.loadSessionObject(sessionKey: key)
                names.append((key, o?.name ?? key))
            }
            catch {
                print("Failed to load session: \(error)")
                do {
                    try wm.deleteSesion(sessionKey: key)
                }
                catch {
                    print("Failed to delete invalid session: \(error)")
                }
            }
        }
        return names
    }

    public func sessionExists(sessionKey: String) throws -> Bool {
        return try self.sessionKeys().contains(sessionKey)
    }

    public func sessionExistsNoThrow(sessionKey: String) -> Bool {
        do {
            return try self.sessionKeys().contains(sessionKey)
        }
        catch {
            return false
        }
    }
}
