//
//  ARCloudWriteError.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

public enum ARCloudWriteError: Error {
    case failedToRead
    case failedToWrite
    case invalidWorldMap
    case error(String)
}
