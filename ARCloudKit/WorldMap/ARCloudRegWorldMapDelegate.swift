//
//  ARCloudRegWorldMapDelegate.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

public protocol ARCloudRegWorldMapDelegate {
    func shareSession(sessionKey: String) throws
    func loadSession(sessionKey: String) throws
    func runInitial() -> Bool
}
