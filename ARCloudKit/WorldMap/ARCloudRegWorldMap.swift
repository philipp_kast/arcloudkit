//
//  ARCloudWorldMap.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

public enum ARCloudRegWorldMapFileExtensions: String {
    case map = "map"
    case metadata = "dat"
    case screenShot = "shot"
}

public enum ARCloudRegVirtualObjectType: String, Codable {
    case information
    case warning
    case question
    case task
    case openTask
    case doneTask
    case gate
}

public class ARCloudRegWorldMapPointOfInterest: Codable {
    public let anchorName: String
    public var type: ARCloudRegVirtualObjectType?
    public var information: String?
    public var x: Float = 0
    public var y: Float = 0
    public var z: Float = 0
    public var eulerAngleX: Float = 0
    public var eulerAngleZ: Float = 0
    public var eulerAngleY: Float = 0

    public init(anchorName: String) {
        self.anchorName = anchorName
    }
}

public class ARCloudRegWorldMapSession: Codable {
    public var key: String
    public var name: String
    public var description: String?
    public var json: Data?
    public var ifcForgeVersionUrn: String?
    public var ifcUrl: String?
    public var ifcFileName: String?
    public var ifcDisplayName: String?
    public var ifcCreateUserName: String?
    public var ifcLastModifiedUserName: String?
    public var ifcCreateTime: String?
    public var ifcLastModifiedTime: String?
    public var ifcScale: Float?
    public var pointsOfInterest = [ARCloudRegWorldMapPointOfInterest]()
    public internal(set) var saved: Bool = false

    public init(key: String, name: String) {
        self.key = key
        self.name = name
    }
}

public class ARCloudRegWorldMapBase: ARCloudDirectory, ARCloudSessionPersictence {
    public var directoryName: String?

    public init() {
    }

    public var fileExtension: String { get { return ARCloudRegWorldMapFileExtensions.map.rawValue } }
    public var metadataFileExtension: String { get { return ARCloudRegWorldMapFileExtensions.metadata.rawValue } }

    public func fileNameForSessionKey(sessionKey: String) -> String {
        return sessionKey + "." + self.fileExtension
    }

    public func metadataFileNameForSessionKey(sessionKey: String) -> String {
        return sessionKey + "." + self.metadataFileExtension
    }

    public func urlForSessionKey(sessionKey: String) -> URL? {
        if let dir = baseURL() {
            return dir.appendingPathComponent(self.fileNameForSessionKey(sessionKey: sessionKey))
        }
        return nil
    }

    public func metadataUrlForSessionKey(sessionKey: String) -> URL? {
        if let dir = baseURL() {
            return dir.appendingPathComponent(self.metadataFileNameForSessionKey(sessionKey: sessionKey))
        }
        return nil
    }

    public func idToIfcFileName(id: String) -> String {
        return "\(id).ifc"
    }

    public func ifcUrlForSessionKey(sessionKey: String) -> URL? {
        do {
            if let session = try self.loadSessionObject(sessionKey: sessionKey) {
                if let urn = session.ifcForgeVersionUrn {
                    if let dir = baseURL() {
                        return dir.appendingPathComponent(idToIfcFileName(id: urn))
                    }
                }
            }
        }
        catch {
            print(error)
        }
        return nil
    }

    public func getScreenShots(sessionKey: String) -> [URL] {
        var screenShotUrls = [URL]()
        let documentsUrl = baseURL()!
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            let files = directoryContents.filter{ $0.lastPathComponent.hasPrefix(sessionKey) && $0.pathExtension == ARCloudRegWorldMapFileExtensions.screenShot.rawValue }
            let fileNames = files.map{ $0.lastPathComponent }
            for fileName in fileNames {
                screenShotUrls.append(documentsUrl.appendingPathComponent(fileName))
            }
        } catch {
            print(error.localizedDescription)
        }
        return screenShotUrls
    }

    public func urlForName(name: String) -> URL? {
        if let dir = baseURL() {
            return dir.appendingPathComponent(name)
        }
        return nil
    }

    public func deleteSesion(sessionKey: String) throws {
        let fm = FileManager.default
        do {
            if let f = self.urlForSessionKey(sessionKey: sessionKey) {
                try fm.removeItem(at: f)
            }
            if let f = self.metadataUrlForSessionKey(sessionKey: sessionKey) {
                try fm.removeItem(at: f)
            }
            for screenShot in self.getScreenShots(sessionKey: sessionKey) {
                try fm.removeItem(at: screenShot)
            }
        }
        catch {
            print("error deleting file \(error)")
            throw ARCloudWriteError.error("Failed to delete file")
        }
    }

    public func nameForSession(sessionKey: String) -> String {
        var name = ""
        do {
            if let s = try self.loadSessionObject(sessionKey: sessionKey) {
                name = s.name
            }
        }
        catch {
            print(error)
            //throw error
        }
        return name
    }

    public func loadSessionObject(sessionKey: String) throws -> ARCloudRegWorldMapSession? {
        var session: ARCloudRegWorldMapSession? = nil
        if let fileURL = self.metadataUrlForSessionKey(sessionKey: sessionKey) {
            let jsonData = try? Data(contentsOf: fileURL, options: .mappedIfSafe)
            if jsonData != nil {
                let jsonObj = try JSONSerialization.jsonObject(with: jsonData!)
                let data1 =  try JSONSerialization.data(withJSONObject: jsonObj, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
                let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
                print("JSON read \(convertedString ?? "")")

                let decoder = JSONDecoder()
                do {
                    session = try decoder.decode(ARCloudRegWorldMapSession.self, from: jsonData!)
                } catch {
                    print("error reading metadata \(error)")
                    throw ARCloudWriteError.failedToRead
                }
            }
        }
        return session
    }
}

public enum ARCloudRegError: Error {
    case error(String)
}

public class ARCloudRegWorldMap : ARCloudRegWorldMapBase, ARCloudRegWorldMapDelegate {
    private let session: ARSession
    public private(set) var currentSession: ARCloudRegWorldMapSession?

    public init(session: ARSession) {
        self.session = session
    }

    public func rerunSession() -> Bool {
        guard ARWorldTrackingConfiguration.isSupported else {
            return false
        }
        let configuration = ARWorldTrackingConfiguration()
        configureTracking(configuration: configuration)
        configuration.worldAlignment = .gravityAndHeading
        self.session.run(configuration)
        return true
    }

    public func runSession() -> Bool {
        guard ARWorldTrackingConfiguration.isSupported else {
            return false
        }
        let configuration = ARWorldTrackingConfiguration()
        configureTracking(configuration: configuration)
        configuration.worldAlignment = .gravityAndHeading
        self.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        return true
    }

    public func runInitial() -> Bool {
        guard self.runSession() else {
            return false
        }

        currentSession = ARCloudRegWorldMapSession(key: "Unsaved", name: "Not Saved Augmentation")
        return true
    }

    public func runInitialLimited() -> Bool {
        guard ARWorldTrackingConfiguration.isSupported else {
            return false
        }
        let configuration = ARWorldTrackingConfiguration()
        configureTracking(configuration: configuration)
        configuration.worldAlignment = .gravity
        self.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])

        currentSession = ARCloudRegWorldMapSession(key: "Unsaved", name: "Not Saved Augmentation")
        return true
    }

    // Writes the current world map to disk
    public func shareSession(sessionKey: String) throws {
        self.session.getCurrentWorldMap { worldMap, error in
            guard let map = worldMap
                else { print("Error: \(error!.localizedDescription)"); return }
            guard let data = try? NSKeyedArchiver.archivedData(withRootObject: map, requiringSecureCoding: true)
                else { fatalError("can't encode map") }
            if let fileURL = self.urlForSessionKey(sessionKey: sessionKey) {
                do {
                    try data.write(to: fileURL)
                }
                catch { fatalError("can't write file" )}
            }
        }
    }

    // Writes the current session to disk
    public func shareSession(session: ARCloudRegWorldMapSession, completionHandler: @escaping (Error?) -> Void) throws {
        self.session.getCurrentWorldMap { worldMap, error in
            guard let map = worldMap else {
                completionHandler(error)
                return
            }
            guard let data = try? NSKeyedArchiver.archivedData(withRootObject: map, requiringSecureCoding: true) else {
                completionHandler(ARCloudRegError.error("Can't encode map"))
                return
            }
            guard let fileURL = self.urlForSessionKey(sessionKey: session.key) else {
                completionHandler(ARCloudRegError.error("Can't get file URL"))
                return
            }
            do {
                try data.write(to: fileURL)
            }
            catch {
                completionHandler(ARCloudRegError.error("Can't write world map file"))
            }
            session.saved = true
            guard let metaFileURL = self.metadataUrlForSessionKey(sessionKey: session.key),
                let encodedData = session.json != nil ? session.json : try? JSONEncoder().encode(session) else {
                completionHandler(ARCloudRegError.error("Can't get meta file URL"))
                return
            }
            do {
            let jsonObj = try JSONSerialization.jsonObject(with: encodedData)
            let data1 =  try JSONSerialization.data(withJSONObject: jsonObj, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
                print("JSON written: \(convertedString ?? "")")
            } catch {}

            do {
                try encodedData.write(to: metaFileURL)
            }
            catch {
                completionHandler(error)
            }
            completionHandler(nil)
        }
    }

    // Reads a session from disk
    // - Reads and loads the world map
    // - Loads the metadata from file and saves it in the current session
    public func loadSession(sessionKey: String) throws {
        try self.loadWorldMap(sessionKey: sessionKey)
        currentSession = try loadSessionObject(sessionKey: sessionKey)
        if currentSession == nil {
            currentSession = ARCloudRegWorldMapSession(key: sessionKey, name: "No name available")
        }
        currentSession?.saved = true
    }

    // Reads and loads the world map
    public func loadWorldMap(sessionKey: String) throws {
        if let fileURL = self.urlForSessionKey(sessionKey: sessionKey) {
            do {
                let data = try Data(contentsOf: fileURL)
                guard let worldMap = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: data)
                    else { throw ARCloudWriteError.invalidWorldMap }
                let configuration = ARWorldTrackingConfiguration()
                configureTracking(configuration: configuration)
                configuration.initialWorldMap = worldMap
                self.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
            }
            catch { throw ARCloudWriteError.failedToRead }
        }
        else {
            throw ARCloudWriteError.failedToRead
        }
    }

    func configureTracking(configuration: ARWorldTrackingConfiguration) {
        configuration.planeDetection = [.horizontal, .vertical]

        /*
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else {
            return
        }
        configuration.detectionImages = referenceImages
        */
    }
}
