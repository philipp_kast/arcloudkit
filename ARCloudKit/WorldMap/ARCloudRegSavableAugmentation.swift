//
//  ARRegSavableAugmentation.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CoreLocation

// Represents an augmentation, a space, which has a world map and a set of items,
// virtual objects anchored in this augmentation
public protocol ARCloudRegSavableAugmentation {

    // Save to disk and upload to the cloud
    func save(
        saveProvider: @escaping (
        _ worldMapUrl: URL,
        _ metadataUrl: URL,
        _ ifcUrl: URL?,
        _ screenShots: [URL],
        _ location: CLLocation?,
        _ pointsOfInterest: [ARCloudRegWorldMapPointOfInterest],
        _ completionHandler: @escaping (Error?) -> Void
        ) -> Void,
        completionHandler: @escaping (Error?) -> Void
    )
}
