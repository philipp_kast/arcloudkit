//
//  ErrorReporter.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 20.09.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

class ErrorReporter {
    class func reportUnexpectedError(message: String) {
        print(message)
    }
}

