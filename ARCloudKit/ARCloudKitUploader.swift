//
//  ARCloudKitUploader.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 20.09.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

// Provides low level functionality to upload data to CloudKit
public class ARCloudKitUploader: ARCloudKitBase {
    public override init(scope: CKDatabase.Scope) {
        super.init(scope: scope)
    }

    // Uploads multiple files into assets of a record type in the public database of CloudKit
    // NOTE: A new record will be created with the given name (or if it exists updated) and will contain the local binary data of the provided URL's
    // recordTypeName
    //      nameField: String
    //      assetFieldN: AssetN   -> Binary data of localFileToUploadIntoAsset
    //      assetListN: AssetsN   -> A list of assets in one field
    public func uploadAssetIntoRecord(
        recordTypeName: String,
        name: String,
        assets: [String: URL],
        assetLists: [String: [URL]] = [:],
        fields: [(String, Any?)]? = nil,
        complete: @escaping (CKRecord) -> Void,
        errorCallback: @escaping (Error) -> Void
    ) {
        self.uploadAssetIntoNewOrUpdateRecord(
            recordTypeName: recordTypeName,
            name: name,
            assets: assets,
            assetLists: assetLists,
            fields: fields,
            complete: complete,
            errorCallback: { error in
                if (nil != ARCloudKitRetry.retryCloudKitOperationIfPossible(
                    with: error,
                    block: {
                        self.uploadAssetIntoRecord(
                            recordTypeName: recordTypeName,
                            name: name,
                            assets: assets,
                            assetLists: assetLists,
                            fields: fields,
                            complete: complete,
                            errorCallback: errorCallback
                        )
                    }
                )) {
                    errorCallback(error)
                }
            }
        )
    }

    private func setRecordValues(
        record: CKRecord,
        assets: [String: URL],
        assetLists: [String: [URL]] = [:],
        fields: [(String, Any?)]? = nil
    ) {
        // Array contains [assetField: String, localFileToUploadIntoAsset: URL]
        for asset in assets {
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: asset.value.path) {
                print("Asset: File does not exist \(asset)")
                continue
            }
            let ckAsset = CKAsset(fileURL: asset.value)
            record[asset.key] = ckAsset
        }
        for assetList in assetLists {
            var a = [CKAsset]()
            for asset in assetList.value {
                let fileManager = FileManager.default
                if !fileManager.fileExists(atPath: asset.path) {
                    print("AssetList: File does not exist \(asset)")
                    continue
                }
                a.append(CKAsset(fileURL: asset))
            }
            record[assetList.key] = a as CKRecordValue
        }
        if let fields = fields {
            for f in fields {
                record[f.0] = f.1 as? __CKRecordObjCValue
            }
        }
    }

    private func uploadAssetIntoNewOrUpdateRecord(
        recordTypeName: String,
        name: String,
        assets: [String: URL],
        assetLists: [String: [URL]] = [:],
        fields: [(String, Any?)]? = nil,
        complete: @escaping (CKRecord) -> Void,
        errorCallback: @escaping (Error) -> Void
    ) {
        createOrGetRecordWithName(recordTypeName: recordTypeName, name: name, complete: { record in
            self.setRecordValues(record: record, assets: assets, assetLists: assetLists, fields: fields)
            self.save(record: record, complete: complete, errorCallback: errorCallback)
        },
        errorCallback: errorCallback)
    }

    // Uploads values into a record type referencing another record type in the public database of CloudKit
    // recordTypeName
    //      nameField: String
    //      referenceField: reference   -> Record type with referenceName as ID
    //      additionalValues...
    public func uploadReferenceIntoNewOrUpdateRecord(
        recordTypeName: String,
        name: String,
        referenceField: String,
        referenceName: String,
        fields: [(String, Any?)]? = nil,
        assets: [String: URL] = [:],
        assetLists: [String: [URL]] = [:],
        complete: @escaping (CKRecord) -> Void,
        errorCallback: @escaping (Error) -> Void
    ) {
        var parentRecordID = CKRecord.ID(recordName: referenceName)
        if scope == CKDatabase.Scope.private {
            let zoneID = CKRecordZone.ID(zoneName: ZoneName, ownerName: CKCurrentUserDefaultName)
            parentRecordID = CKRecord.ID(recordName: referenceName, zoneID: zoneID)
        }
        let reference = CKRecord.Reference(recordID: parentRecordID, action: .deleteSelf)

        createOrGetRecordWithName(recordTypeName: recordTypeName, name: name, complete: { record in
            record[referenceField] = reference
            self.setRecordValues(record: record, assets: assets, assetLists: assetLists, fields: fields)
            self.save(record: record, complete: complete, errorCallback: errorCallback)
        },
        errorCallback: errorCallback)
    }

    private func queryByReference(
        recortTypeName: String,
        zone: CKRecordZone.ID?,
        referenceName: String,
        reference: CKRecord.Reference,
        completionHandler: @escaping ([CKRecord]?, Error?) -> Void
    ) {
        let predicate = NSPredicate(format: "\(referenceName) == %@", reference)
        let query = CKQuery(recordType: recortTypeName, predicate: predicate)
        self.database.perform(query, inZoneWith: zone, completionHandler: { records, error in
            completionHandler(records, error)
        })
    }

    private func createOrGetRecordWithName(
        recordTypeName: String,
        name: String,
        complete: @escaping (CKRecord) -> Void,
        errorCallback: @escaping (Error) -> Void
    ) {
        var recordID = CKRecord.ID(recordName: name)
        if scope == CKDatabase.Scope.private {
            let zoneID = CKRecordZone.ID(zoneName: ZoneName, ownerName: CKCurrentUserDefaultName)
            recordID = CKRecord.ID(recordName: name, zoneID: zoneID)
        }

        self.database.fetch(withRecordID: recordID) { (record, error) in
            if let error = error, error._code != 11 {
                let message = "Error save data: \(error)"
                ErrorReporter.reportUnexpectedError(message: message)
                errorCallback(error)
                return
            }
            if record == nil {
                let record = CKRecord(recordType: recordTypeName, recordID: recordID)
                complete(record)
            }
            else {
                complete(record!)
            }
        }
    }

    private func save(
        record: CKRecord,
        complete: @escaping (CKRecord) -> Void,
        errorCallback: @escaping (Error) -> Void
    ) {
        self.database.save(record) {
            (record, error) in
            if let error = error {
                let message = "Error save data: \(error)"
                ErrorReporter.reportUnexpectedError(message: message)
                errorCallback(error)
                return
            }
            if let r = record {
                complete(r)
            }
            else {
                errorCallback(ARCloudAugmentationError.error("No record recived"))
            }
        }
    }
}
