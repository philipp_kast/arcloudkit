//
//  CloudKitRetry.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 09.11.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

class ARCloudKitRetry {
    /// Helper method to retry a CloudKit operation when its error suggests it
    ///
    /// - Parameters:
    ///   - error: The error returned from a CloudKit operation
    ///   - block: A block to be executed after a delay if the error is recoverable
    /// - Returns: If the error can't be retried, returns the error
    class func retryCloudKitOperationIfPossible(with error: Error?,
                                                block: @escaping () -> ()) -> Error? {
        guard let effectiveError = error as? CKError else {
            // not a CloudKit error or no error present, just return the original error
            return error
        }

        guard let retryAfter = retryAfterSeconds(effectiveError: effectiveError) else {
            return effectiveError
        }

        // Schedule `block` to be executed later
        DispatchQueue.main.asyncAfter(deadline: .now() + retryAfter) {
            block()
        }

        return nil
    }

    class func retryAfterSeconds(effectiveError: CKError) -> Double? {
        if effectiveError.code == CKError.networkFailure || effectiveError.code == CKError.networkUnavailable {
            // Network not available, retry in 30 seconds
            return 30
        }
        guard let retryAfter = effectiveError.retryAfterSeconds else {
            // CloudKit error, can't  be retried, return the error
            return nil
        }
        return retryAfter
    }
}
