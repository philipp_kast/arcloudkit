//
//  UserDefaultsExtension.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 09.10.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

extension UserDefaults {

    func getServerChangeToken(key: String) -> CKServerChangeToken? {
        guard let data = self.value(forKey: key) as? Data else {
            return nil
        }
        guard let token = try? NSKeyedUnarchiver.unarchivedObject(ofClass: CKServerChangeToken.self, from: data) else {
            return nil
        }

        return token
    }

    func setServerChangeToken(key: String, newValue: CKServerChangeToken?) {
        if let token = newValue {
            let data = try? NSKeyedArchiver.archivedData(withRootObject: token, requiringSecureCoding: true)
            self.set(data, forKey: key)
            self.synchronize()
        }
        else {
            self.removeObject(forKey: key)
        }
    }

    var serverChangeToken: CKServerChangeToken? {
        get {
            guard let data = self.value(forKey: "ChangeToken") as? Data else {
                return nil
            }
            guard let token = try? NSKeyedUnarchiver.unarchivedObject(ofClass: CKServerChangeToken.self, from: data) else {
                return nil
            }

            return token
        }
        set {
            if let token = newValue {
                let data = try? NSKeyedArchiver.archivedData(withRootObject: token, requiringSecureCoding: true)
                self.set(data, forKey: "ChangeToken")
                self.synchronize()
            } else {
                self.removeObject(forKey: "ChangeToken")
            }
        }
    }

    var serverChangeTokenZone: CKServerChangeToken? {
        get {
            guard let data = self.value(forKey: "ChangeTokenZone") as? Data else {
                return nil
            }
            guard let token = try? NSKeyedUnarchiver.unarchivedObject(ofClass: CKServerChangeToken.self, from: data) else {
                return nil
            }

            return token
        }
        set {
            if let token = newValue {
                let data = try? NSKeyedArchiver.archivedData(withRootObject: token, requiringSecureCoding: true)
                self.set(data, forKey: "ChangeTokenZone")
                self.synchronize()
            } else {
                self.removeObject(forKey: "ChangeTokenZone")
            }
        }
    }
}
