//
//  SCNVector3Extension.swift
//  ARRegister
//
//  Created by Philipp Kast on 05.05.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import SceneKit

func +(left:SCNVector3, right:SCNVector3) -> SCNVector3 {
    return SCNVector3(left.x + right.x, left.y + right.y, left.z + right.z)
}

func -(left:SCNVector3, right:SCNVector3) -> SCNVector3 {
    return left + (right * -1.0)
}

func *(vector:SCNVector3, multiplier:SCNFloat) -> SCNVector3 {
    return SCNVector3(vector.x * multiplier, vector.y * multiplier, vector.z * multiplier)
}

/**
 * Increments a SCNVector3 with the value of another.
 */
func += ( left: inout SCNVector3, right: SCNVector3) {
    left = left + right
}

/**
 * Decrements a SCNVector3 with the value of another.
 */
func -= ( left: inout SCNVector3, right: SCNVector3) {
    left = left - right
}

/**
 * Multiplies two SCNVector3 vectors and returns the result as a new SCNVector3.
 */
func * (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x * right.x, left.y * right.y, left.z * right.z)
}

/**
 * Multiplies a SCNVector3 with another.
 */
func *= ( left: inout SCNVector3, right: SCNVector3) {
    left = left * right
}

/**
 * Divides two SCNVector3 vectors abd returns the result as a new SCNVector3
 */
func / (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x / right.x, left.y / right.y, left.z / right.z)
}

/**
 * Divides a SCNVector3 by another.
 */
func /= ( left: inout SCNVector3, right: SCNVector3) {
    left = left / right
}

/**
 * Divides the x, y and z fields of a SCNVector3 by the same scalar value and
 * returns the result as a new SCNVector3.
 */
func / (vector: SCNVector3, scalar: Float) -> SCNVector3 {
    return SCNVector3Make(vector.x / scalar, vector.y / scalar, vector.z / scalar)
}

/**
 * Divides the x, y and z of a SCNVector3 by the same scalar value.
 */
func /= ( vector: inout SCNVector3, scalar: Float) {
    vector = vector / scalar
}

extension SCNVector3 {
    func dotProduct(_ vector:SCNVector3) -> SCNFloat {
        return (x * vector.x) + (y * vector.y) + (z * vector.z)
    }

    // perpendicular to both vectors and thus normal to the plane containing them
    func crossProduct(_ vector: SCNVector3) -> SCNVector3 {
        return SCNVector3(y * vector.z - z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x)
    }

    var flat:SCNVector3 {
        get {
            return SCNVector3(x, 0.0, z)
        }
    }
    var magnitude:SCNFloat {
        get {
            return sqrt(dotProduct(self))
        }
    }

    static func positionFromTransform(_ transform: matrix_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
    }

    var normalized:SCNVector3 {
        get {
            let localMagnitude = magnitude
            let localX = x / localMagnitude
            let localY = y / localMagnitude
            let localZ = z / localMagnitude

            return SCNVector3(localX, localY, localZ)
        }
    }

    func roundedTo(precision:Float) -> SCNVector3 {
        if precision == 0.0 { return self }
        let curLength = length()
        let multiplier = Float(1.0 / precision)
        let newLength = round(multiplier * (curLength + precision/2.0)) / multiplier
        return SCNVector3(x*newLength/curLength, y*newLength/curLength, z*newLength/curLength)
    }

    func length() -> Float {
        return sqrtf(x*x + y*y + z*z)
    }

    // length of vector
    func distance(to: SCNVector3) -> Float {
        return (self - to).length()
    }

    func mid(to: SCNVector3) -> SCNVector3 {
        return SCNVector3((self.x + to.x) / 2, (self.y + to.y) / 2, (self.z + to.z) / 2)
    }

    // perpendicular distance to line on floor (xz-plane)
    func minFlatDistance(lineFrom: SCNVector3, lineTo: SCNVector3) -> Float {
        var val = (lineTo.z - lineFrom.z)*x - (lineTo.x - lineFrom.x)*z + lineTo.x*lineFrom.z - lineTo.z*lineFrom.x
        //print(String(format:"Distance: %f", val))
        val = val / (sqrt((lineTo.x - lineFrom.x)*(lineTo.x - lineFrom.x) + (lineTo.z - lineFrom.z)*(lineTo.z - lineFrom.z)))
        //print(String(format:"Normalized Distance: %f", val))
        return val
    }

    func perpendicularIntersectWith(lineFrom: SCNVector3, lineTo: SCNVector3) -> SCNVector3 {
        let angle = ((lineTo - lineFrom)).angleY((self - lineFrom))
        let normalHitPt = lineFrom + (lineTo - lineFrom) * (cos(.pi*angle/180.0) * (self - lineFrom).length() / (lineTo - lineFrom).length())
        return normalHitPt
    }

    func shift(dx: Float, dz:Float) -> SCNVector3 {
        return SCNVector3(x + dx, y, z + dz)
    }

    func rotY(pivot: SCNVector3, degrees:Float) -> SCNVector3 {
        let toCenter = SCNVector3(x - pivot.x, y, z - pivot.z)
        let radians = degrees * Float.pi / 180.0
        let cs = cos(radians)
        let sn = sin(radians)
        let centerRot = SCNVector3(toCenter.x * cs - toCenter.z * sn, y, toCenter.x * sn + toCenter.z * cs)
        return SCNVector3(centerRot.x + pivot.x, y, centerRot.z + pivot.z)
    }

    //    Get Angle between vectors between -180 and +180 in degrees (clockwise)
    //           0
    //    -90         90
    //          180
    func angleY(_ vector:SCNVector3) -> SCNFloat { // result in degrees
        let v1 = SCNVector3(x,0.0,z)
        let v2 = SCNVector3(vector.x, 0.0, vector.z)
        let bearingRadians =  atan2(v2.z, v2.x) - atan2(v1.z, v1.x);
        var bearingDegrees = bearingRadians * 180.0 / Float.pi
        while bearingDegrees < -180.0 {
            bearingDegrees += 360.0
        }
        while bearingDegrees > 180.0 {
            bearingDegrees -= 360.0
        }
        return bearingDegrees
    }

    static func lineEulerAngles(vector: SCNVector3) -> SCNVector3 {
        let height = vector.length()
        let lxz = sqrtf(vector.x * vector.x + vector.z * vector.z)
        let pitchB = vector.y < 0 ? Float.pi - asinf(lxz/height) : asinf(lxz/height)
        let pitch = vector.z == 0 ? pitchB : sign(vector.z) * pitchB

        var yaw: Float = 0
        if vector.x != 0 || vector.z != 0 {
            let inner = vector.x / (height * sinf(pitch))
            if inner > 1 || inner < -1 {
                yaw = Float.pi / 2
            } else {
                yaw = asinf(inner)
            }
        }
        return SCNVector3(CGFloat(pitch), CGFloat(yaw), 0)
    }
}

/*
extension SCNVector3: Codable {

    private enum CodingKeys: String, CodingKey {
        case x
        case y
        case z
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.x = try container.decode(Float.self, forKey: .x)
        self.y = try container.decode(Float.self, forKey: .y)
        self.z = try container.decode(Float.self, forKey: .z)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.x, forKey: .x)
        try container.encode(self.y, forKey: .y)
        try container.encode(self.z, forKey: .z)
    }
}
*/
