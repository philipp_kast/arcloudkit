//
//  ARCloudKitBase.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 18.11.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

public class ARCloudKitBase {
    let scope: CKDatabase.Scope

    public init(scope: CKDatabase.Scope) {
        self.scope = scope
    }

    var database: CKDatabase {
        get {
            let myContainer = CKContainer.default()
            switch self.scope {
            case CKDatabase.Scope.private:
                return myContainer.privateCloudDatabase
            case CKDatabase.Scope.shared:
                return myContainer.sharedCloudDatabase
            default:
                return myContainer.publicCloudDatabase
            }
        }
    }
}
