//
//  PrivateDataSyncer.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 27.09.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

let ZoneName = "Augmentations"

// Allows to sync data from iCloud
public class ARCloudDataSyncer {
    let privateSubscriptionId = "private-changes"
    #if SHARD_DATABSE_SUPPORT
    let sharedSubscriptionId = "shared-changes"
    #endif
    #if PUBLIC_DATABSE_SUPPORT
    let publicSubscriptionId = "public-changes"
    #endif

    let createdCustomZoneKey = "createdCustomZone"
    let subscribedToPrivateChangesKey = "subscribedToPrivateChanges"
    #if SHARD_DATABSE_SUPPORT
    var subscribedToSharedChangesKey = "subscribedToSharedChanges"
    #endif
    #if PUBLIC_DATABSE_SUPPORT
    var subscribedToPublicChangesKey = "subscribedToPublicChanges"
    #endif

    let privateDB: CKDatabase
    #if SHARD_DATABSE_SUPPORT
    let sharedDB: CKDatabase
    #endif
    #if PUBLIC_DATABSE_SUPPORT
    let publicDB: CKDatabase
    #endif

    let zoneID: CKRecordZone.ID

    let createZoneGroup = DispatchGroup()

    let onRecordModified: (CKRecord) -> Void
    let onRecordDeleted: ((CKRecord.ID, CKRecord.RecordType)) -> Void

    private var changedRecords = [CKRecord]()
    private var deletedRecords = [(CKRecord.ID, CKRecord.RecordType)]()

    // Instantiate the class with callbacks which will be called on data update and deletion
    // Note: This only happens on the private data
    public init(onRecordModified: @escaping (CKRecord) -> Void, onRecordDeleted: @escaping ((CKRecord.ID, CKRecord.RecordType)) -> Void) {
        self.onRecordModified = onRecordModified
        self.onRecordDeleted = onRecordDeleted

        let container = CKContainer.default()
        privateDB = container.privateCloudDatabase
        #if SHARD_DATABSE_SUPPORT
        sharedDB = container.sharedCloudDatabase
        #endif
        #if PUBLIC_DATABSE_SUPPORT
        publicDB = container.publicCloudDatabase
        #endif
        // Use a consistent zone ID across the user's devices
        // CKCurrentUserDefaultName specifies the current user's ID when creating a zone ID
        zoneID = CKRecordZone.ID(zoneName: ZoneName, ownerName: CKCurrentUserDefaultName)
    }

    // Subscribes to change notifications and does the initialization
    // NOTE: This has to be called initally
    public func subscribeToChangeNotifications(completion: @escaping (Error?, CKDatabase.Scope) -> Void) {
        self.createCutomZone()

        if !self.subscribedToPrivateChanges {
            let createSubscriptionOperation = self.createDatabaseSubscriptionOperation(subscriptionId: privateSubscriptionId)
            createSubscriptionOperation.modifySubscriptionsCompletionBlock = { (subscriptions, deletedIds, error) in
                if error == nil {
                    self.subscribedToPrivateChanges = true
                }
                // else custom error handling
            }
            self.privateDB.add(createSubscriptionOperation)
        }

        #if SHARD_DATABSE_SUPPORT
        if !self.subscribedToSharedChanges {
            let createSubscriptionOperation = self.createDatabaseSubscriptionOperation(subscriptionId: sharedSubscriptionId)
            createSubscriptionOperation.modifySubscriptionsCompletionBlock = { (subscriptions, deletedIds, error) in
                if error == nil {
                    self.subscribedToSharedChanges = true
                }
                // else custom error handling
            }
            self.sharedDB.add(createSubscriptionOperation)
        }
        #endif

        #if PUBLIC_DATABSE_SUPPORT
        if !self.subscribedToPublicChanges {
            let createSubscriptionOperation = self.createDatabaseSubscriptionOperation(subscriptionId: publicSubscriptionId)
            createSubscriptionOperation.modifySubscriptionsCompletionBlock = { (subscriptions, deletedIds, error) in
                if error == nil {
                    self.subscribedToPublicChanges = true
                }
                // else custom error handling
            }
            self.publicDB.add(createSubscriptionOperation)
        }
        #endif

        // Fetch any changes from the server that happened while the app wasn't running
        createZoneGroup.notify(queue: DispatchQueue.global()) {
            if self.createdCustomZone {
                self.fetchChanges(in: .private) { error in
                    completion(error, .private)
                }
                #if SHARD_DATABSE_SUPPORT
                self.fetchChanges(in: .shared) { error in
                    completion(error, .shared)
                }
                #endif
                #if PUBLIC_DATABSE_SUPPORT
                self.fetchChanges(in: .public) { error in
                    completion(error, .public)
                }
                #endif
            }
        }
    }

    // Fetches changes which happened since the last fetch
    public func fetchChanges(in databaseScope: CKDatabase.Scope, completion: @escaping (Error?) -> Void) {
        switch databaseScope {
        case .private:
            fetchDatabaseChanges(database: self.privateDB, scope: .private, completion: completion)
        #if SHARD_DATABSE_SUPPORT
        case .shared:
            fetchDatabaseChanges(database: self.sharedDB, scope: .shared, completion: completion)
        #endif
        #if PUBLIC_DATABSE_SUPPORT
        case .public:
            fetchDatabaseChanges(database: self.publicDB, scope: .public, completion: completion)
        #endif
        default:
            fatalError()
        }
    }

    var createdCustomZone: Bool {
        get {
            let userSettings = UserDefaults.standard
            return userSettings.bool(forKey: createdCustomZoneKey)
        }
        set {
            let userSettings = UserDefaults.standard
            userSettings.set(newValue, forKey: createdCustomZoneKey)
        }
    }

    var subscribedToPrivateChanges: Bool {
        get {
            let userSettings = UserDefaults.standard
            return userSettings.bool(forKey: subscribedToPrivateChangesKey)
        }
        set {
            let userSettings = UserDefaults.standard
            userSettings.set(newValue, forKey: subscribedToPrivateChangesKey)
        }
    }

    #if SHARD_DATABSE_SUPPORT
    var subscribedToSharedChanges: Bool {
        get {
            let userSettings = UserDefaults.standard
            return userSettings.bool(forKey: subscribedToSharedChangesKey)
        }
        set {
            let userSettings = UserDefaults.standard
            userSettings.set(newValue, forKey: subscribedToSharedChangesKey)
        }
    }
    #endif

    #if PUBLIC_DATABSE_SUPPORT
    var subscribedToPublicChanges: Bool {
        get {
            let userSettings = UserDefaults.standard
            return userSettings.bool(forKey: subscribedToPublicChangesKey)
        }
        set {
            let userSettings = UserDefaults.standard
            userSettings.set(newValue, forKey: subscribedToPublicChangesKey)
        }
    }
    #endif

    private func scopeToString(scope: CKDatabase.Scope) -> String {
        switch scope {
        case .private:
            return "private"
        case .shared:
            return "shared"
        default:
            return "public"
        }
    }

    func readChangeTokenFromDisk(scope: CKDatabase.Scope) -> CKServerChangeToken? {
    #if !SYNC_INCREMENTAL_DISABLED
        let userSettings = UserDefaults.standard
        return userSettings.getServerChangeToken(key: "serverChangeToken_" + scopeToString(scope: scope))
    #else
        return nil
    #endif
    }

    func writeChangeTokenToDisk(scope: CKDatabase.Scope, token: CKServerChangeToken?) {
        let userSettings = UserDefaults.standard
        userSettings.setServerChangeToken(key: "serverChangeToken_" + scopeToString(scope: scope), newValue: token)
    }

    func readChangeTokenForZoneFromDisk(scope: CKDatabase.Scope) -> CKServerChangeToken? {
    #if !SYNC_INCREMENTAL_DISABLED
        let userSettings = UserDefaults.standard
        return userSettings.getServerChangeToken(key: "serverChangeTokenZone_" + scopeToString(scope: scope))
    #else
        return nil
    #endif
    }

    func writeChangeTokenForZoneToDisk(scope: CKDatabase.Scope, token: CKServerChangeToken?) {
        let userSettings = UserDefaults.standard
        userSettings.setServerChangeToken(key: "serverChangeTokenZone_" + scopeToString(scope: scope), newValue: token)
    }

    func createCutomZone() {
        if !self.createdCustomZone {
            createZoneGroup.enter()

            let customZone = CKRecordZone(zoneID: zoneID)

            let createZoneOperation = CKModifyRecordZonesOperation(recordZonesToSave: [customZone], recordZoneIDsToDelete: [] )

            createZoneOperation.modifyRecordZonesCompletionBlock = { (saved, deleted, error) in
                if (error == nil) {
                    self.createdCustomZone = true
                }
                // else custom error handling
                self.createZoneGroup.leave()
            }
            createZoneOperation.qualityOfService = .userInitiated

            self.privateDB.add(createZoneOperation)
        }
    }

    func fetchDatabaseChanges(database: CKDatabase, scope: CKDatabase.Scope, completion: @escaping (Error?) -> Void) {
        var changedZoneIDs: [CKRecordZone.ID] = []

        let changeToken = readChangeTokenForZoneFromDisk(scope: scope)
        let operation = CKFetchDatabaseChangesOperation(previousServerChangeToken: changeToken)

        operation.recordZoneWithIDChangedBlock = { (zoneID) in
            changedZoneIDs.append(zoneID)
        }

        operation.recordZoneWithIDWasDeletedBlock = { (zoneID) in
            // Write this zone deletion to memory
        }

        operation.changeTokenUpdatedBlock = { (token) in
            // Flush zone deletions for this database to disk
            // Write this new database change token to memory
        }

        operation.fetchDatabaseChangesCompletionBlock = { (token, moreComing, error) in
            if let error = error {
                print("Error during fetch shared database changes operation", error)
                completion(error)
                return
            }
            // Flush zone deletions for this database to disk
            // Write this new database change token to memory
            if changedZoneIDs.count == 0 {
                completion(nil)
                return
            }
            self.fetchZoneChanges(database: database, token: token, scope: scope, zoneIDs: changedZoneIDs) {error in
                // Flush in-memory database change token to disk
                if error == nil {
                    self.writeChangeTokenForZoneToDisk(scope: scope, token: token)
                }
                completion(error)
            }
        }
        operation.qualityOfService = .userInitiated

        database.add(operation)
    }

    func fetchZoneChanges(database: CKDatabase, token: CKServerChangeToken?, scope: CKDatabase.Scope, zoneIDs: [CKRecordZone.ID], completion: @escaping (Error?) -> Void) {
        // Look up the previous change token for each zone
        var optionsByRecordZoneID = [CKRecordZone.ID: CKFetchRecordZoneChangesOperation.ZoneConfiguration]()
        for zoneID in zoneIDs {
            let options = CKFetchRecordZoneChangesOperation.ZoneConfiguration()
            let t = readChangeTokenFromDisk(scope: scope)
            options.previousServerChangeToken = t
            optionsByRecordZoneID[zoneID] = options
        }
        let operation = CKFetchRecordZoneChangesOperation()
        operation.recordZoneIDs = zoneIDs
        operation.configurationsByRecordZoneID = optionsByRecordZoneID

        operation.recordChangedBlock = { (record) in
            print("Record changed:", record)
            // Write this record change to memory
            self.changedRecords.append(record)
        }

        operation.recordWithIDWasDeletedBlock = { (recordId, recordType) in
            print("Record deleted:", recordId)
            // Write this record deletion to memory
            self.deletedRecords.append((recordId, recordType))
        }

        operation.recordZoneChangeTokensUpdatedBlock = { (zoneId, token, data) in
            // Flush record changes and deletions for this zone to disk
            self.flushChanges()
            // Write this new zone change token to disk
            self.writeChangeTokenToDisk(scope: scope, token: token)
        }

        operation.recordZoneFetchCompletionBlock = { (zoneId, changeToken, _, _, error) in
            if let error = error {
                print("Error fetching zone changes for \(self.scopeToString(scope: scope)) database:", error)
                return
            }
            // Flush record changes and deletions for this zone to disk
            self.flushChanges()
            // Write this new zone change token to disk
            self.writeChangeTokenToDisk(scope: scope, token: changeToken)
        }

        operation.fetchRecordZoneChangesCompletionBlock = { (error) in
            if let error = error {
                print("Error fetching zone changes for \(self.scopeToString(scope: scope)) database:", error)
            }
            completion(error)
        }

        database.add(operation)
    }

    func flushChanges()  {
        for record in self.changedRecords {
            onRecordModified(record)
        }
        self.changedRecords.removeAll()
        for (recordId, recordType) in self.deletedRecords {
            onRecordDeleted((recordId, recordType))
        }
        self.deletedRecords.removeAll()
    }

    func createDatabaseSubscriptionOperation(subscriptionId: String) -> CKModifySubscriptionsOperation {
        let subscription = CKDatabaseSubscription.init(subscriptionID: subscriptionId)

        let notificationInfo = CKSubscription.NotificationInfo()
        // send a silent notification
        notificationInfo.shouldSendContentAvailable = true
        subscription.notificationInfo = notificationInfo

        let operation = CKModifySubscriptionsOperation(subscriptionsToSave: [subscription], subscriptionIDsToDelete: [])
        operation.qualityOfService = .utility

        return operation
    }
}
