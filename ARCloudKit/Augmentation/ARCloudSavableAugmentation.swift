//
//  ARCloudSavableAugmentation.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 25.10.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CoreLocation
import CloudKit

// Represents an augmentation, a space, which has a world map and a set of items,
// virtual objects anchored in this augmentation
public protocol ARCloudSavableAugmentation {
   // Upload to the cloud
    func save(scope: CKDatabase.Scope, completionHandler: @escaping (Error?) -> Void)
}
