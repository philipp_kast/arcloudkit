//
//  ARCloudAugmentationUtil.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 22.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit
import ARKit

public class ARCloudAugmentationUtil {
    // For all augmentation items referencing an augmentation id
    // downloads assets and returns json data
    public class func downloadAugmentationItemAssets(
        id: String,
        cacheDirectory: String,
        scope: CKDatabase.Scope,
        complete: @escaping ([String?], [ARCloudKitDownloader.DownloadedAsset]) -> Void
    ) {
        var downloadedAssets = [ARCloudKitDownloader.DownloadedAsset]()
        let downloader = ARCloudKitDownloader(scope: scope)
        let group = DispatchGroup()
        group.enter()
        downloader.downloadAllAssetsOfReference(
            recordTypeName: ARCloudAugmentationConfig.AugmentationItemRecordTypeName,
            referenceField: ARCloudAugmentationConfig.AugmentationReferenceName,
            referenceName: id,
            directoryName: cacheDirectory,
            assetFields: [ARCloudAugmentationConfig.ItemImageFieldName: "jpeg"],
            complete: { assets in
                print("Assets downloaded")
                downloadedAssets = assets
                group.leave()
            },
            errorCallback: { _ in
                print("Assets download failed")
                //group.leave()
        })
        group.enter()
        var jsonData = [String?]()
        downloader.downloadAllValuesOfReference(
            recordTypeName: ARCloudAugmentationConfig.AugmentationItemRecordTypeName,
            referenceField: ARCloudAugmentationConfig.AugmentationReferenceName,
            referenceName: id,
            valueNamesToRead: [
                ARCloudAugmentationConfig.ItemJsonDataFieldName
            ],
            completed: { recordValues in
                for values in recordValues {
                    jsonData.append(values[ARCloudAugmentationConfig.ItemJsonDataFieldName] as? String)
                }
                group.leave()
        })
        group.wait()
        complete(jsonData, downloadedAssets)
    }

    // Saves an augmentation with including the world map and uploads to the cloud
    public class func saveAugmentation(
        id: String,
        name: String,
        augmentationName: String,
        jsonData: Data,
        location: CLLocation?,
        address: String,
        city: String,
        country: String,
        session: ARSession,
        cacheDirectory: String,
        scope: CKDatabase.Scope,
        items: [ARCloudAugmentationItem]
    ) {
        let worldMap = ARCloudRegWorldMap(session: session)
        worldMap.directoryName = cacheDirectory
        let worldMapSession = ARCloudRegWorldMapSession(key: id, name: name)
        worldMapSession.json = jsonData
        let a = ARCloudRegAugmentation(
            worldMapSession: worldMapSession,
            worldMap: worldMap,
            screenShots: [],
            location: location)
        a.save(
            saveProvider: { worldMapUrl,
                metadataUrl,
                ifcUrl,
                screenShots,
                location,
                pointsOfInterest,
                completionHandler in
                let a = ARCloudAugmentation(key: worldMapSession.key,
                                            name: augmentationName,
                                            worldMap: worldMapUrl,
                                            metadata: metadataUrl,
                                            ifc: ifcUrl,
                                            augmentationItems: items,
                                            screenShots: screenShots,
                                            location: location,
                                            address: address,
                                            city: city,
                                            country: country
                )
                a.save(scope: scope, completionHandler: { error in
                    completionHandler(error)
                })
            },
            completionHandler: { error in
                if let e = error {
                    print(e)
                }
        })
    }

    // Uploads an augmentation to the cloud
    public class func uploadAugmentation(
        id: String,
        augmentationName: String,
        location: CLLocation?,
        address: String,
        city: String,
        country: String,
        cacheDirectory: String,
        scope: CKDatabase.Scope,
        items: [ARCloudAugmentationItem]
    ) {
        let worldMap = ARCloudRegWorldMapBase()
        worldMap.directoryName = cacheDirectory
        if
            let worldMapUrl = worldMap.urlForSessionKey(sessionKey: id),
            let metadataUrl = worldMap.metadataUrlForSessionKey(sessionKey: id)
        {
            let ifcUrl = worldMap.ifcUrlForSessionKey(sessionKey: id)
            let a = ARCloudAugmentation(
                key: id,
                name: augmentationName,
                worldMap: worldMapUrl,
                metadata: metadataUrl,
                ifc: ifcUrl,
                augmentationItems: items,
                screenShots: [],
                location: location,
                address: address,
                city: city,
                country: country
            )
            a.save(scope: CKDatabase.Scope.public, completionHandler: { error in
                if let error = error {
                    print("Failed to make public: \(error)")
                }
                else {
                    print("Succeeded to make public")
                }
            })
        }
    }
}
