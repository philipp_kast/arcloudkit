//
//  ARCloudAugmentationError.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 27.10.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

public enum ARCloudAugmentationError: Error {
    case error(String)
}

