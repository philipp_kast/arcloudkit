//
//  AugmentationConfig.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 03.11.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

public class ARCloudAugmentationConfig {
    public static var AugmentationRecordTypeName = "Augmentation"
    public static var AugmentationItemRecordTypeName = "AugmentationItem"
    public static var AugmentationReferenceName = "augmentation"
    public static var MetadataFieldName = "metadata"
    public static var IfcFieldName = "ifc"
    public static var WorldMapFieldName = "worldMap"
    public static var NameFieldName = "name"
    public static var LocationFieldName = "location"
    public static var ScreenShotsFieldName = "screenShots"
    public static var AddressFieldName = "address"
    public static var CityFieldName = "city"
    public static var CountryFieldName = "country"
    public static var ItemJsonDataFieldName = "jsonData"
    public static var ItemImageFieldName = "image"
}
