//
//  AugmentationItem.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 27.10.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

public class ARCloudAugmentationItem: Codable {
    public let id: String
    public var type: ARCloudAugmentationItemType?
    public var assets: [String: URL] = [:]
    // add the following:
    // public var spaceId: String = "" // uuid of Space
    // public var version: Int = 1 // version of json data structure
    // public var asset: Asset // file for img, 3d, html?
    
    // delete the following
    public var jsonData: String?

    public init(id: String) {
        self.id = id
    }

    // Save item, creates a new record or updates the existing with with id
    public func save(scope: CKDatabase.Scope, parent: String) {
        let cloudKitUploader = ARCloudKitUploader(scope: scope)
        cloudKitUploader.uploadReferenceIntoNewOrUpdateRecord(
            recordTypeName: ARCloudAugmentationConfig.AugmentationItemRecordTypeName,
            name: self.id,
            referenceField: ARCloudAugmentationConfig.AugmentationReferenceName,
            referenceName: parent,
            fields: [
                ("type", self.type?.rawValue ?? ""),
                (ARCloudAugmentationConfig.ItemJsonDataFieldName, self.jsonData ?? "")
            ],
            assets: assets,
            complete: { record in
                print("Augmentation item saved")
        }, errorCallback: { error in
            print(error)
        })
    }

    public func delete(scope: CKDatabase.Scope) {
        ARCloudKitDeleter(scope: scope).delete(name: id,
                                               completed: { print("Item deleted") },
                                               errorCallback: { error in print(error) })
    }
}
