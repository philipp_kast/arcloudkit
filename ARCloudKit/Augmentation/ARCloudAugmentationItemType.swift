//
//  AugmentationType.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 27.10.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation

public enum ARCloudAugmentationItemType: String, Codable {
    case spot // Point, POI, Marker, Anchor
    case route // line / multiline
    case zone // horizontal area
    case picture // vertical rectangular image area, loaded from asset URL
    case building // immobile element that belongs to building structure
    case entity // immobile, stationary equipment
    case object // 3D object
    case gate // gateway to a next AR session
}

public enum ARCloudSpotAugmentationItemType {
    case information
    case warning
    case question
    case task
    case openTask
    case doneTask
}
