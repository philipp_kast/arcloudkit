//
//  ARCloudAugmentation
//  ARCloudKit
//
//  Created by Philipp Kast on 26.10.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CoreLocation
import CloudKit
import CoreLocation

public class ARCloudAugmentation: ARCloudSavableAugmentation {
    let key: String
    let name: String
    let worldMap: URL
    let metadata: URL // jsonData
    let ifc: URL? // IFC file
    let augmentationItems: [ARCloudAugmentationItem]
    let screenShots: [URL]
    let location: CLLocation?
    let address: String?
    let city: String?
    let country: String?

    public init(
        key: String,
        name: String,
        worldMap: URL,
        metadata: URL,
        ifc: URL?,
        augmentationItems: [ARCloudAugmentationItem],
        screenShots: [URL],
        location: CLLocation?,
        address: String? = nil,
        city: String? = nil,
        country: String? = nil
    ) {
        self.key = key
        self.name = name
        self.worldMap = worldMap
        self.metadata = metadata
        self.augmentationItems = augmentationItems
        self.screenShots = screenShots
        self.location = location
        self.address = address
        self.city = city
        self.country = country
        self.ifc = ifc
    }

    public func save(
        scope: CKDatabase.Scope,
        completionHandler: @escaping (Error?) -> Void
    ) {
        let cloudKitUploader = ARCloudKitUploader(scope: scope)
        var assets = [
            ARCloudAugmentationConfig.WorldMapFieldName: worldMap,
            ARCloudAugmentationConfig.MetadataFieldName: metadata,
        ]
        if let ifc = ifc {
            assets.updateValue(ifc, forKey: ARCloudAugmentationConfig.IfcFieldName)
        }
        cloudKitUploader.uploadAssetIntoRecord(
            recordTypeName: ARCloudAugmentationConfig.AugmentationRecordTypeName,
            name: key,
            assets: assets,
            assetLists: [
                ARCloudAugmentationConfig.ScreenShotsFieldName: screenShots
            ],
            fields: [
                (ARCloudAugmentationConfig.LocationFieldName, location),
                (ARCloudAugmentationConfig.NameFieldName, self.name),
                (ARCloudAugmentationConfig.AddressFieldName, self.address),
                (ARCloudAugmentationConfig.CityFieldName, self.city),
                (ARCloudAugmentationConfig.CountryFieldName, self.country)
            ],
            complete: { record in
                for augmentationItem in self.augmentationItems {
                    augmentationItem.save(scope: scope, parent: self.key)
                }
                print("Success: written to iCloud")
                completionHandler(nil)
            }, errorCallback: { error in
                print("Failed: written to iCloud \(error)")
                completionHandler(error)
            }
        )
    }
}
