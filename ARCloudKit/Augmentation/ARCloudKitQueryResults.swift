//
//  ARCloudQueryResults.swift
//  ARCloudKit
//
//  Created by Philipp Kast on 02.12.18.
//  Copyright © 2018 Philipp Kast. All rights reserved.
//

import Foundation
import CloudKit

public class ARCloudKitQueryResults {
    public let recordID: CKRecord.ID
    public let values: [String]

    init(recordID: CKRecord.ID, values: [String]) {
        self.recordID = recordID
        self.values = values
    }
}
