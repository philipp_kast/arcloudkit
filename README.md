# ARCloudKit, ARCloud via CloudKit

Implements an AR cloud based on CloudKit.

## Features
- Upload, download and delete records from CloudKit
- CloudKit synchronization

## Setup

[Setup example](doc/Setup.md)
